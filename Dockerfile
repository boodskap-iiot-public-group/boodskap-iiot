FROM openjdk:jdk

RUN mkdir -p /opt/boodskap
WORKDIR /opt/boodskap
ARG PLATFORM_TYPE
ARG PLATFORM_VERSION
ENV BSKP_VERSION=${PLATFORM_TYPE}-${PLATFORM_VERSION}
ENV BOODSKAP_HOME=/opt/boodskap
COPY ./boodskap-${BSKP_VERSION}.tar.gz /opt/boodskap/
RUN tar -xzvf boodskap-${BSKP_VERSION}.tar.gz && rm boodskap-${BSKP_VERSION}.tar.gz
RUN echo "java -jar /opt/boodskap/bin/boodskap-iiot-${PLATFORM_VERSION}.jar" > /opt/boodskap/bin/startup.sh
RUN chmod +x /opt/boodskap/bin/startup.sh

EXPOSE 18080 19090

CMD ["which", "java"]
ENTRYPOINT ["/bin/bash", "/opt/boodskap/bin/startup.sh"]
