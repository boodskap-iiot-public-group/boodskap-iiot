package io.boodskap.iot;

import java.io.File;
import java.lang.reflect.Method;

import groovy.lang.GroovyClassLoader;

public class Bootstrap {
	
	static final String HOME = "BOODSKAP_HOME";

	static final GroovyClassLoader gcl = new GroovyClassLoader();
	
	public Bootstrap() {
	}
	
	private static String getPropOrEnv(String key, String def) {
		
		String value = System.getProperty(HOME);
		
		if(null == value) {
			value = System.getenv(HOME);
		}
		
		return null != value ? value : def;
	}
	
	private static File getHomeDir() {
		
		String home = getPropOrEnv(HOME, System.getProperty("user.home"));
		
		File homeDir = new File(home);
		
		if(!new File(homeDir, "libs").exists()) {
			homeDir = new File(".");
		}
		
		return homeDir;
	}

	public static void main(String[] args) {
		
		try {
			
			File homeDir = getHomeDir();
			File cfgDir = new File(homeDir, "config");
			File libDir = new File(homeDir, "libs");
			File patchDir = new File(homeDir, "patches");
			File tinyLog = new File(cfgDir, "tinylog.properties");
			File logDir = new File(cfgDir, "logs");
			
			System.out.format("Home: %s\n", homeDir.getAbsolutePath());
			System.out.format("Config: %s\n", cfgDir.getAbsolutePath());
			System.out.format("Libs: %s\n", libDir.getAbsolutePath());
			System.out.format("Patches: %s\n", patchDir.getAbsolutePath());
			
			System.setProperty("tinylog.configuration", tinyLog.getAbsolutePath());
			
			logDir.mkdirs();
			
			File[] patchFiles = patchDir.listFiles(File::isFile);
			File[] libFiles = libDir.listFiles(File::isFile);
			
			if(null != patchFiles) {
				for(File f : patchFiles) {
					//System.out.format("Adding %s\n", f.getAbsolutePath());
					gcl.addClasspath(f.getAbsolutePath());
				}
			}
			
			if(null != libFiles) {
				for(File f : libFiles) {
					//System.out.format("Adding %s\n", f.getAbsolutePath());
					gcl.addClasspath(f.getAbsolutePath());
				}
			}
			
			Class<?> clazz = gcl.loadClass("io.boodskap.iot.Main");
			Method main = clazz.getMethod("main", String[].class);
			
			main.invoke(null, (Object)args);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	}
}
