#!/bin/bash
PLATFORM=$1
VERSION=$2
docker push boodskapiotplatform/platform-${PLATFORM}:latest
docker push boodskapiotplatform/platform-${PLATFORM}:${VERSION}
