#!/bin/bash
PLATFORM=$1
VERSION=$2
docker build -f Dockerfile --build-arg PLATFORM_TYPE=${PLATFORM} --build-arg PLATFORM_VERSION=${VERSION} -t boodskapiotplatform/platform-${PLATFORM}:latest -t boodskapiotplatform/platform-${PLATFORM}:${VERSION} ./target/${PLATFORM}
